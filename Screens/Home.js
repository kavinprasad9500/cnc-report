import {
  SafeAreaView,
  ScrollView,
  StyleSheet,
  View,
} from "react-native";
import React, { useContext, useEffect, useState } from "react";
import { useFocusEffect } from "@react-navigation/native";
import { useTheme } from "../components/Theme";
import { Text } from "@react-native-material/core";
import MyProduct from "../components/MyProduct";
import FloatingAddButton from "../components/FloatingAddButton";
import { AppContext } from "../components/MyContext";
import NavigateBar from "../components/NavigateBar";
import { BackHandler, Alert } from "react-native";
import { fetchDataFromApi } from "../config/APiConfig";
import { ActivityIndicator } from "@react-native-material/core";
import Background from "../components/Background";

const Home = ({ navigation }) => {
  const { primary } = useTheme();

  const { accessAdmin, setAdmin } = useContext(AppContext);
  const { studentName, setStudentName } = useContext(AppContext);
  const [isLoading, setIsLoading] = useState(true); // State to track loading status

  const [data, setData] = useState([]);

  useEffect(() => {
    // Fetch data from the API when the component mounts
    fetchData();
  }, [data]);

  const fetchData = async () => {
    const requestOptions = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        /* Your data goes here */
      }),
      redirect: "follow",
    };

    const apiEndpoint = "product/list_product";

    try {
      const jsonData = await fetchDataFromApi(apiEndpoint, requestOptions);
      if (Array.isArray(jsonData)) {
        setData(jsonData);
      } else {
        console.error("Invalid data format received from API:", jsonData);
      }
    } catch (error) {
      console.error("Error fetching data:", error);
      Alert.alert("Error", "Please Check Network");
      
      setData([]);
    } finally {
      setIsLoading(false);
    }
  };

  useFocusEffect(
    React.useCallback(() => {
      const onBackPress = () => {
        Alert.alert("Confirm Exit", "Are you sure you want to exit the app?", [
          {
            text: "Cancel",
            onPress: () => null,
            style: "cancel",
          },
          { text: "OK", onPress: () => BackHandler.exitApp() },
        ]);
        return true;
      };

      BackHandler.addEventListener("hardwareBackPress", onBackPress);

      return () =>
        BackHandler.removeEventListener("hardwareBackPress", onBackPress);
    }, [])
  );

  const handleProductPress = (key) => {
    if (accessAdmin) navigation.navigate("EditProduct", { key });
    else navigation.navigate("Process", { key });
  };
  const styles = StyleSheet.create({
    scrollViewContainer: {
      flexDirection: "row",
      flexWrap: "wrap",
      display: "flex",
      justifyContent: "space-between",
    },
    addProduct: {
      color: primary,
    },
  });

  return (
    <SafeAreaView
      style={{
        paddingHorizontal: 20,
        backgroundColor: "#DBE9F6",
        width: "100%",
        height: "100%",
      }}
    >
    <Background/>

      <Text variant="h6" style={{ color: primary, marginVertical: 10 }}>
        <Text variant="h5">Welcome,</Text> {studentName}
      </Text>
      <NavigateBar
        activityNav={() => navigation.navigate("Activities")}
        componentTab
      />
      {isLoading ? (
        <ActivityIndicator size="large" color={primary} />
      ) : (
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.scrollViewContainer}>
            {data.length === 0 ? (
              <Text variant="h6" style={{ textAlign: "center" }}>
                No products available
              </Text>
            ) : (
              data.map((item) => (
                <MyProduct
                  key={item.product_id}
                  productName={item.product_name}
                  machine={item.machine_name}
                  onPress={() => handleProductPress(item.product_id)}
                />
              ))
            )}
          </View>
        </ScrollView>
      )}
      {accessAdmin ? (
        <FloatingAddButton onPress={() => navigation.navigate("AddProduct")} />
      ) : null}
    </SafeAreaView>
  );
};
export default Home;
