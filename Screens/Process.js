import { Alert, SafeAreaView, ScrollView, ToastAndroid } from "react-native";
import React, { useContext, useEffect, useState } from "react";
import { useRoute } from "@react-navigation/native";
import { useTheme } from "../components/Theme";
import MyInput from "../components/MyInput";
import { Text } from "@react-native-material/core";
import MyButton from "../components/MyButton";
import { AppContext } from "../components/MyContext";
import { fetchDataFromApi } from "../config/APiConfig";
import { ActivityIndicator } from "@react-native-material/core";
import Background from "../components/Background";


const Home = ({ navigation }) => {
  const { primary, fontStyles } = useTheme();
  const route = useRoute();
  const key = route.params?.key;

  const [data, setData] = useState([]);
  const [productName, setProductName] = useState("");
  const [isLoading, setIsLoading] = useState(true); // State to track loading status

  const { cartItems, setCartItems } = useContext(AppContext);
  const [enteredData, setEnteredData] = useState(() => {
    const initialInputs = data.map(() => "");
    return initialInputs;
  });

  useEffect(() => {
    // Fetch data from the API when the component mounts
    getProductName();
    fetchData();
  }, []);

  const getProductName = async () => {
    const requestOptionsDelete = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({}),
      redirect: "follow",
    };

    const apiEndpointProduct = `/details/get_product?id=${key}`;

    try {
      const productNameJson = await fetchDataFromApi(
        apiEndpointProduct,
        requestOptionsDelete
      );
      setProductName(productNameJson[0].product_name);
    } catch (error) {
      setProductName(null);
      Alert.alert("Error", "Please Check Network");
      console.error("Error fetching data:", error);
    } finally {
      // Set loading to false once the API request is completed (whether successful or not)
      setIsLoading(false);
    }
  };

  const fetchData = async () => {
    const requestOptions = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        /* Your data goes here */
      }),
      redirect: "follow",
    };

    const apiEndpoint = "product/list_process?id=" + key;

    try {
      const jsonData = await fetchDataFromApi(apiEndpoint, requestOptions);

      if (Array.isArray(jsonData)) {
        setData(jsonData);
      } else {
        console.error("Invalid data format received from API:", jsonData);
      }
    } catch (error) {
      setData([]);
      console.error("Error fetching data:", error);
    }
  };

  const handleAddToCart = () => {
    // Filter out processes with empty or undefined inputs and create the cart item object
    const cartItemData = data
      .filter((process, index) => enteredData[index] !== "")
      .map((process, index) => ({
        productId: key,
        processId: process.process_id,
        count: enteredData[index],
      }));

    if (cartItemData.length > 0) {
      //   // Create the new cart item with the key and processes
      //   const newCartItem = {
      //     key: key,
      //     processes: cartItemData,
      //   };
      // Todo: if process already exists add only count
      // Append the new cart item to cartItems array using setCartItems
      setCartItems((prevCartItems) => [...prevCartItems, cartItemData]);
      ToastAndroid.show("Items Added To Cart", ToastAndroid.SHORT);

    }

    // console.log(cartItemData); // Check if cartItemData is correctly formatted

    navigation.navigate("Home");
  };

  return (
    <SafeAreaView
      style={{
        paddingHorizontal: 20,
        backgroundColor: "#DBE9F6",
        width: "100%",
        height: "100%",
      }}
    >
    <Background/>
      <Text
        variant="h5"
        style={{
          color: primary,
          ...fontStyles.bold,
          fontSize: 20,
          marginVertical: 15,
        }}
      >
        <Text style={{ fontSize: 20 }}>Product Name : </Text>
        {productName}
      </Text>
      <ScrollView showsVerticalScrollIndicator={false}>
        {isLoading ? (
          <ActivityIndicator size="large" color={primary} />
        ) : data.length === 0 ? (
          <Text variant="h6" style={{ textAlign: "center" }}>
            No Process available
          </Text>
        ) : (
          data.map((process, index) => (
            <MyInput
              keyboardType={true}
              key={`${process.process_id}_${index}_${process.productId}`}
              InputStyle={{ marginVertical: 5 }}
              label={process.process_name}
              leading="check-network"
              leadingIconColor={primary}
              value={enteredData[index]} // Use the separate state for entered data
              onTextChange={(newText) => {
                const updatedData = [...enteredData];
                updatedData[index] = newText;
                setEnteredData(updatedData);
              }}
            />
          ))
        )}
      </ScrollView>
      <MyButton
        buttonStyle={{ marginVertical: 20 }}
        title="Add to Cart"
        icon="cart-plus"
        onPress={handleAddToCart}
      />
    </SafeAreaView>
  );
};

export default Home;
