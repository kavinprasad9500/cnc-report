import {
  Alert,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  ToastAndroid,
  View,
} from "react-native";
import React, { useEffect, useMemo, useState } from "react";
import { useTheme } from "../components/Theme";
import MyInput from "../components/MyInput";
import { Text } from "@react-native-material/core";
import MyStudentList from "../components/MyStudentList";
import NavigateBar from "../components/NavigateBar";
import { fetchDataFromApi } from "../config/APiConfig";
import { ActivityIndicator } from "@react-native-material/core";
import Background from "../components/Background";

const Activities = ({ navigation }) => {
  const { primary } = useTheme();
  const [searchStudent, setsearchStudent] = useState("");
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(true); // State to track loading status

  useEffect(() => {
    fetchData();
    // console.log(data);
  }, [data]);

  const fetchData = async () => {
    const requestOptions = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        /* Your data goes here */
      }),
      redirect: "follow",
    };

    const apiEndpoint = "students/list_students";

    try {
      const jsonData = await fetchDataFromApi(apiEndpoint, requestOptions);
      if (Array.isArray(jsonData)) {
        // Format the date to "24 Jul 23" format
        const formattedData = jsonData.map((item) => ({
          ...item,
          date: formatDate(item.date),
        }));
        setData(formattedData);
      } else {
        console.error("Invalid data format received from API:", jsonData);
      }
    } catch (error) {
      // Handle errors here
      Alert.alert("Error", "Please Check Network");
      console.error("Error fetching data:", error);
      setData([]);
    } finally {
      // Set loading to false once the API request is completed (whether successful or not)
      setIsLoading(false);
    }
  };

  const formatDate = (dateString) => {
    const date = new Date(dateString);
    const options = { day: "numeric", month: "short", year: "2-digit" };
    return date.toLocaleDateString(undefined, options);
  };
  const handleSearch = (searchText) => {
    setsearchStudent(searchText);
    // console.log(searchText);
  };
  
  const filterdata = useMemo(() => {
    return data.filter(
      (item) =>
        item.student_name.toLowerCase().includes(searchStudent.toLowerCase()) ||
        item.date.toLowerCase().includes(searchStudent.toLowerCase())
    );
  }, [data, searchStudent]);

  const handleDeleteStudent = async (id) => {
    // console.log(id);
    const requestOptions = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        /* Your data goes here */
      }),
      redirect: "follow",
    };

    const apiEndpoint = `students/delete?id=${id}`;

    try {
      const deleteData = await fetchDataFromApi(apiEndpoint, requestOptions);
      if (deleteData.message == "Success") {
        ToastAndroid.show("Activity Deleted", ToastAndroid.SHORT);
        navigation.navigate("Activities");
      } else {
        console.error("Invalid data format received from API:", deleteData);
      }
    } catch (error) {
      // Handle errors here
      Alert.alert("Error", "Please Check Network");
      console.error("Error fetching data:", error);
      ToastAndroid.show(error, ToastAndroid.SHORT);

    }
  };

  const handleInfoStudent = async (id, date) =>{
    navigation.navigate('StudentActivities',{id,date});
  };

  return (
    <SafeAreaView
      style={{
        paddingHorizontal: 20,
        backgroundColor: "#DBE9F6",
        width: "100%",
        height: "100%",
      }}
    >
    <Background/>
   
      <MyInput
        value={searchStudent}
        leading="account-search"
        label="Search Student"
        InputStyle={{ marginVertical: 10 }}
        leadingIconColor={primary}
        onTextChange={handleSearch}
      />
      <NavigateBar componentsNav={() => navigation.navigate("Home")} activityTab/>

      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.scrollViewContainer}>
        {isLoading ? (
          <ActivityIndicator size="large" color={primary} />
        ) :
          filterdata.length === 0 ? (
            <Text variant="h6">No Students Data Found</Text>
          ) : (
            filterdata.map((item) => (
              <MyStudentList
                // key={item.id}
                studentName={item.student_name}
                date={item.date}
                email={item.email}
                onView={() => handleInfoStudent(item.student_id,item.date)}
                onDelete={() => handleDeleteStudent(item.student_id)}
              />
            ))
          )}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default Activities;

const styles = StyleSheet.create({
  scrollViewContainer: {
    flexDirection: "row",
    flexWrap: "wrap",
    display: "flex",
    justifyContent: "space-between",
    alignItems: 'center',
    justifyContent: 'center'
  },
});
