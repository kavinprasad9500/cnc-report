import { View, Image, Dimensions, StyleSheet } from "react-native";
import { useTheme } from "../components/Theme";
import MyButton from "../components/MyButton";
import MyInput from "../components/MyInput";
import { useContext, useState } from "react";
import { Text } from "@react-native-material/core";
import { AppContext } from "../components/MyContext";
import Background from "../components/Background";

const Login = ({ navigation }) => {
  const { fontStyles, textBold, textMedium } = useTheme();
  const { accessAdmin, setAdmin } = useContext(AppContext);
  const { studentName, setStudentName } = useContext(AppContext);

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(false);

  const handleUsernameChange = (newUsername) => {
    setUsername(newUsername);
    setStudentName(newUsername);
  };

  const handlePasswordChange = (newPassword) => {
    setPassword(newPassword);
  };

  const Sumbit = () => {
    // console.log(username);
    // console.log(password);
    // if (username == "admin" && password == "Admin@esec098") {
      if (username == "admin" ) {
      setAdmin(true);
      // console.log(accessAdmin);
      navigation.navigate("Home");
    } else {
      if (username && password == "cnc@esec") {
        // if (username != "admin" ) {
        setError(false);
        setAdmin(false);
        navigation.navigate("Home");
      } else setError(true);
    }
  };

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      paddingHorizontal: 20,
      backgroundColor: "#DBE9F6",
      justifyContent: "center",
      alignItems: "center",
      width: "100%",
      height: "100%",
    },
    headerText: {
      color: textBold,
      ...fontStyles.bold,
      marginVertical: "10%",
    },
    subHeaderText: {
      color: textMedium,
      ...fontStyles.bold,
    },
    image: {
      width: 100,
      height: 100,
    },
  });

  return (
    <View style={styles.container}>
      <Background />
      <Image
        style={styles.image}
        source={require("../assets/welcome-icon.png")}
      />
      <Text variant="h6" style={styles.headerText}>
        Your Must Login to Report
      </Text>
      <MyInput
        InputStyle={{ marginVertical: 10 }}
        label="Student Name"
        leading="account"
        value={username}
        onTextChange={handleUsernameChange}
      />
      <MyInput
        InputStyle={{ marginVertical: 10 }}
        label="Password"
        leading="lock"
        trailing="eye"
        value={password}
        onTextChange={handlePasswordChange}
        secureTextEntry={true}
      />
      {error ? (
        <Text style={{ color: "#ff0000", fontSize: 14 }}>
          Incorrect Password Please Contact Admin
        </Text>
      ) : null}

      <MyButton
        buttonStyle={{ marginTop: 30 }}
        title="LOGIN"
        onPress={Sumbit}
      />
    </View>
  );
};

export default Login;
