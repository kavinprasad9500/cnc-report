import { View, Image, Dimensions, StyleSheet, StatusBar } from "react-native";
import { Text } from "@react-native-material/core";
import { useTheme } from "../components/Theme";
import MyButton from "../components/MyButton";
import Background from "../components/Background";

const screenWidth = Dimensions.get("window").width;

const GetStart = ({ navigation }) => {
  const { primary, fontStyles } = useTheme();

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      paddingHorizontal: 20,
      backgroundColor: "#DBE9F6",
      alignItems: "center",
      justifyContent: "center",
      width: "100%",
      height: "100%",
    },
    headerText: {
      color: "#13244E",
      ...fontStyles.bold,
      justifyContent: "center",
      alignItems: "center",
      textAlign: "center",
    },
    headerTextColored: {
      color: primary,
    },
    subHeaderText: {
      color: "#45566B",
      ...fontStyles.bold,
      textAlign: 'center'
    },
    image: {
      width: screenWidth,
      height: "45%",
    },
  });

  return (
    <View style={styles.container}>
    <Background/>

      <Text variant="h4" style={styles.headerText}>
        <Text variant="h3" style={styles.headerTextColored}></Text>
        Welcome to Production Reporting App!
      </Text>
      <Image
        source={require("../assets/GetStart.png")}
        style={styles.image}
        resizeMode="contain"
      />
      <Text variant="h6" style={styles.subHeaderText}>
        Keep track of your production{'\n'} ease and efficiency.
      </Text>

      <MyButton
        buttonStyle={{ marginTop: 30 }}
        title="GET STARTED"
        onPress={() => navigation.navigate("Login")}
      />
      {/* <Button
        icon={<Icon name="arrow-right" size={15} color="white" />}
        title="Button with icon component"
        type="clear"
      /> */}
    </View>
  );
};

export default GetStart;
