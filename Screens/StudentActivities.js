import { SafeAreaView, ScrollView, Linking, Alert } from "react-native";
import React, { useEffect, useState } from "react";
import { useTheme } from "../components/Theme";
import { Text } from "@react-native-material/core";
import MyButton from "../components/MyButton";
import CartProduct from "../components/CartProduct";
import { useRoute } from "@react-navigation/native";
import { fetchDataFromApi } from "../config/APiConfig";
import { ActivityIndicator } from "@react-native-material/core";
import Background from "../components/Background";

const StudentActivities = ({ navigation }) => {
  const { primary, fontStyles } = useTheme();
  const route = useRoute();
  const studentId = route.params?.id;
  const date = route.params?.date;
  const [studentName, setStudentName] = useState("");
  const [cartItems, setCartItems] = useState([]);
  const [isLoading, setIsLoading] = useState(true); // State to track loading status

  useEffect(() => {
    // Fetch data from the API when the component mounts
    getStudentName();
    studentActivities(studentId);
    // console.log(cartItems);
  }, []); // Empty dependency array to fetch data only once on mount

  const getStudentName = async () => {
    const requestOptionsStudent = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({}),
      redirect: "follow",
    };

    const apiEndpointStudent = `/details/get_student?id=${studentId}`;

    try {
      const studentNameJson = await fetchDataFromApi(
        apiEndpointStudent,
        requestOptionsStudent
      );
      setStudentName(studentNameJson[0].student_name);
    } catch (error) {
      setStudentName(null);
      Alert.alert("Error", "Please Check Network");
      console.error("Error fetching data:", error);
    }
  };

  const getProductName = async (id) => {
    const requestOptionsProduct = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({}),
      redirect: "follow",
    };

    const apiEndpointProduct = `/details/get_product?id=${id}`;

    try {
      const productNameJson = await fetchDataFromApi(
        apiEndpointProduct,
        requestOptionsProduct
      );
      return {
        product_name: productNameJson[0].product_name,
        machine_name: productNameJson[0].machine_name,
      };
    } catch (error) {
      return null;
      Alert.alert("Error", "Please Check Network");
      console.error("Error fetching data:", error);
    }
  };

  const getProcess = async (id) => {
    const requestOptionsProcess = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({}),
      redirect: "follow",
    };

    const apiEndpointProcess = `/details/get_process?id=${id}`;

    try {
      const processJson = await fetchDataFromApi(
        apiEndpointProcess,
        requestOptionsProcess
      );
      return processJson[0];
    } catch (error) {
      return null;
      Alert.alert("Error", "Please Check Network");
      console.error("Error fetching data:", error);
    }
  };

  const studentActivities = async (studentId) => {
    const requestOptions = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        /* Your data goes here */
      }),
      redirect: "follow",
    };

    const apiEndpointSelect = `students/list_student_activites?id=${studentId}`;

    try {
      const jsonData = await fetchDataFromApi(
        apiEndpointSelect,
        requestOptions
      );
      if (Array.isArray(jsonData)) {
        const activitiesPromises = jsonData.map(async (item) => {
          const processJson = await getProcess(item.process_id);
          if (!processJson) {
            // Handle case when processJson is null (e.g., API error)
            return null;
          }
          const productName = await getProductName(processJson.product_id);
          return {
            product_name: productName.product_name,
            machineName: productName.machine_name,
            process_name: processJson.process_name,
            count: item.count,
            cost: processJson.cost * item.count, // Access the cost from processJson
            time: parseFloat((processJson.time * item.count) / 60).toFixed(2),
          };
        });

        const formattedData = await Promise.all(activitiesPromises);
        // Filter out any null values (in case of API errors) before setting the cartItems
        const filteredData = formattedData.filter((data) => data !== null);
        setCartItems(filteredData);
      }
    } catch (error) {
      Alert.alert("Error", "Please Check Network");
      console.error("Error fetching data:", error);
    } finally {
      // Set loading to false once the API request is completed (whether successful or not)
      setIsLoading(false);
    }
  };

  const confirmEmail = async (id) => {
    const requestOptionsProcess = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({}),
      redirect: "follow",
    };

    const apiEndpoint = `/students/email?student_id=${id}`;

    try {
      const processJson = await fetchDataFromApi(
        apiEndpoint,
        requestOptionsProcess
      );
      if (processJson.message == "Success") return true;
      else return false;
    } catch (error) {
      Alert.alert("Error", "Please Check Network");
      console.error("Error fetching data:", error);
    }
  };

  const confirmRegister = () => {
    confirmEmail(studentId);
    openEmailApp();
    navigation.navigate("Activities");
  };
  const openEmailApp = async () => {
    const emailSubject = `Machinery Report From ${studentName}`;
    // console.log(cartItems);
    const emailBody = `
Name: ${studentName}
Date: ${date}
In Time: 09:00 AM
Out Time: 04:30 PM

Delay: Lunch 30 Minutes

Products Done:
_______________________________________
${cartItems
  .map(
    (item, index) =>
      `${index + 1}. Product Name: ${item.product_name}
      Process: ${item.process_name}
      Count: ${item.count} nos
      Cost: ${item.cost}
      Time: ${item.time}
_______________________________________\n`
  )
  .join("")}

Total Time: ${totalTimeFormatted}
Total Cost: ${totalCost.toFixed(2)}
`;

    const recipientEmail = "esecprincipal@gmail.com"; // Replace this with the desired recipient email address
    const mailtoUrl = `mailto:${recipientEmail}?subject=${encodeURIComponent(
      emailSubject
    )}&body=${encodeURIComponent(emailBody)}`;

    Linking.canOpenURL(mailtoUrl)
      .then((supported) => {
        if (!supported) {
          console.log("Opening email is not supported on this device.");
        } else {
          return Linking.openURL(mailtoUrl);
        }
      })
      .catch((err) =>
        console.error("An error occurred while opening the email app:", err)
      );
  };

  const totalTimeInMinutes = cartItems.reduce(
    (total, item) => total + parseFloat(item.time),
    0
  );
  const totalCost = cartItems.reduce((total, item) => total + item.cost, 0);

  // Convert total time to hours or minutes format
  const totalHours = Math.floor(totalTimeInMinutes / 60);
  const totalMinutes = totalTimeInMinutes % 60;
  const totalTimeFormatted =
    totalHours > 0
      ? `${totalHours} (hrs)${totalHours > 1 ? "s" : ""}`
      : `${totalMinutes.toFixed(2)} (min)`;

  return (
    <SafeAreaView
      style={{
        paddingHorizontal: 20,
        backgroundColor: "#DBE9F6",
        width: "100%",
        height: "100%",
      }}
    >
      <Background />

      <Text
        variant="h5"
        style={{
          color: primary,
          ...fontStyles.bold,
          fontSize: 20,
          marginVertical: 10,
        }}
      >
        <Text style={{ fontSize: 20 }}>Student Name : </Text>
        {studentName}
      </Text>

      <ScrollView showsVerticalScrollIndicator={false}>
        {isLoading ? (
          <ActivityIndicator size="large" color={primary} />
        ) : cartItems.length === 0 ? (
          <Text variant="h6">No Students Data Found</Text>
        ) : (
          <>
            {cartItems.map((item, index) => (
              <CartProduct
                key={index} // Use the index as a key since it's unique
                productName={item.product_name}
                processName={item.process_name}
                machineName={item.machineName}
                count={item.count}
                cost={
                  item.cost >= 1000
                    ? (item.cost / 1000).toFixed(1) + "k"
                    : item.cost
                }
                timeTaken={item.time}
                style={{ marginVertical: 5 }}
              />
            ))}
            <Text variant="h6" style={{ marginTop: 10 }}>
              Total Time : {"\t"}
              <Text variant="h6" style={{ color: primary }}>
                {totalTimeFormatted}
              </Text>
            </Text>
            <Text variant="h6">
              Total Cost : {"\t"}
              <Text variant="h6" style={{ color: primary }}>
                ₹{totalCost.toFixed(2)}
              </Text>
            </Text>
          </>
        )}
      </ScrollView>
      <MyButton
        buttonStyle={{ marginBottom: 10 }}
        title="Send Mail"
        icon="paper-plane"
        onPress={confirmRegister}
      />
    </SafeAreaView>
  );
};

export default StudentActivities;
