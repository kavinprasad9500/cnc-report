import React, { useState } from "react";
import { Text, StyleSheet, SafeAreaView, ToastAndroid, Alert } from "react-native";
import MyInput from "../components/MyInput";
import MyInputGroup from "../components/MyInputGroup";
import MyButton from "../components/MyButton";
import { Button } from "react-native-elements";
import Icon from "react-native-vector-icons/FontAwesome";
import { fetchDataFromApi } from "../config/APiConfig";
import Background from "../components/Background";

const AddProduct = ({ navigation }) => {
  const [productName, setProductName] = useState("");
  const [machineName, setMachineName] = useState("");
  const [processCount, setProcessCount] = useState(1);
  const [errorMessage, setErrorMessage] = useState("");

  const handleProductNameChange = (productName) => {
    setProductName(productName);
  };
  const handleMachineNameChange = (machineName) => {
    setMachineName(machineName);
  };

  const handleAddProduct = () => {
    // Prepare the process data as a string in the required format
    if (!productName.trim()) {
      setErrorMessage("Please enter a product name");
      return;
    }
    for (const process of data.processData) {
      if (!process.processName.trim() || !process.time || !process.cost) {
        setErrorMessage("Please fill in all process details");
        return;
      }
    }
    const processString = JSON.stringify(
      data.processData.map((item) => [item.processName, item.time, item.cost])
    );

    // Create the FormData object to send the data
    var formdata = new FormData();
    formdata.append("product_name", productName);
    formdata.append("machine_name", machineName.toUpperCase());
    formdata.append("process", processString);

    // Set up the fetch options
    var requestOptions = {
      method: "POST",
      body: formdata,
      redirect: "follow",
    };

    // console.log(productName);
    // console.log(data.processData);

    // Make the API call
    const fetchData = async () => {
      const apiEndpoint = "product/new";

      try {
        const jsonData = await fetchDataFromApi(apiEndpoint, requestOptions);
        // console.log(jsonData.Message);
        if (jsonData.Message) {
          ToastAndroid.show("Product Added", ToastAndroid.SHORT);
          navigation.navigate("Home");
        } else {
          console.error("Invalid data format received from API:", jsonData);
          
        }
      } catch (error) {
        // Handle errors here
        Alert.alert("Error", "Please Check Network");
        console.error("Error fetching data:", error);
        ToastAndroid.show(error, ToastAndroid.SHORT);
      }
    };

    // Call the fetchData function
    fetchData();
  };

  const [data, setData] = useState({
    processData: [{ processName: "", time: "", cost: "" }],
  });

  // Function to handle the input change for process names and time costs
  const handleProcessChange = (index, value, field) => {
    setData((prevData) => ({
      ...prevData,
      processData: prevData.processData.map((item, i) =>
        i === index ? { ...item, [field]: value } : item
      ),
    }));
  };

  // Function to add a new process input group
  const handleAddProcess = () => {
    setData((prevData) => ({
      ...prevData,
      processData: [
        ...prevData.processData,
        { processName: "", time: "", cost: "" },
      ],
    }));
    setProcessCount((prevCount) => prevCount + 1);
  };
  const handleDeleteProcess = (index) => {
    setData((prevData) => ({
      ...prevData,
      processData: prevData.processData.filter((_, i) => i !== index),
    }));
    setProcessCount((prevCount) => prevCount - 1);
  };

  // Generate an array of process input groups based on the processCount state
  const renderProcessGroups = () => {
    const processGroups = [];
    for (let i = 1; i <= processCount; i++) {
      processGroups.push(
        <MyInputGroup
          key={i}
          style={styles.inputBox}
          label1={`Process Name ${i}`}
          onTextChange={(name) =>
            handleProcessChange(i - 1, name, "processName")
          }
          onTextChange1={(time) => handleProcessChange(i - 1, time, "time")}
          onTextChange2={(cost) => handleProcessChange(i - 1, cost, "cost")}
          // onDelete={() => handleDeleteProcess(i)}
        />
      );
    }
    return processGroups;
  };

  return (
    <SafeAreaView
      style={{
        paddingHorizontal: 20,
        backgroundColor: "#DBE9F6",
        width: "100%",
        height: "100%",
        alignItems: "baseline",
      }}
    >
    <Background/>

      <MyInput
        label="Product Name"
        InputStyle={{ marginVertical: 10 }}
        leading="file"
        leadingIconColor="#7350B8"
        value={productName}
        onTextChange={handleProductNameChange}
      />
      <MyInput
        label="Machine Name"
        InputStyle={{ marginVertical: 10 }}
        leading="slot-machine"
        leadingIconColor="#7350B8"
        value={machineName}
        onTextChange={handleMachineNameChange}
      />
      {renderProcessGroups()}
      {errorMessage ? (
        <Text style={styles.errorText}>{errorMessage}</Text>
      ) : null}
      <Button
        titleStyle={{ color: "#7350B8", fontSize: 18 }}
        title="  Add Process"
        icon={<Icon name="plus" size={12} color="#7350B8" />}
        type="clear"
        onPress={handleAddProcess}
      />

      <MyButton
        buttonStyle={{ marginVertical: 20 }}
        title="Add Product"
        onPress={handleAddProduct}
      />
    </SafeAreaView>
  );
};

export default AddProduct;

const styles = StyleSheet.create({
  inputBox: {
    marginVertical: 5,
  },
  errorText: {
    color: "red",
    fontSize: 14,
    marginBottom: 10,
  },
});
