import React, { useEffect, useState } from "react";
import { Text, StyleSheet, SafeAreaView, ScrollView, ToastAndroid, View, Alert } from "react-native";
import MyInput from "../components/MyInput";
import MyInputGroup from "../components/MyInputGroup";
import MyButton from "../components/MyButton";
import { fetchDataFromApi } from "../config/APiConfig";
import { useRoute } from "@react-navigation/native";
import { ActivityIndicator } from "@react-native-material/core";
import { useTheme } from "../components/Theme";
import Background from "../components/Background";


const EditProduct = ({ navigation }) => {
  const [productName, setProductName] = useState("");
  const [machineName, setMachineName] = useState("");
  const route = useRoute();
  const productId = route.params?.key;

  const [selectData, setSelectData] = useState([]);
  const [updatedData, setUpdatedData] = useState([]);
  const [isLoading, setIsLoading] = useState(true); // State to track loading status

  const { primary } = useTheme();

  useEffect(() => {
    // Fetch data from the API when the component mounts
    getProductName();
    fetchData();
  }, []); // Empty dependency array to fetch data only once on mount

  useEffect(() => {
    // Update updatedData whenever selectData changes
    setUpdatedData(selectData);
  }, [selectData]); // Dependency array includes selectData, so it will update on selectData change

  const handleProductNameChange = (productName) => {
    setProductName(productName);
  };
  const handleMachineNameChange = (machineName) => {
    setMachineName(machineName);
  };

  const getProductName = async () => {
    const requestOptionsDelete = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({}),
      redirect: "follow",
    };

    const apiEndpointProduct = `/details/get_product?id=${productId}`;

    try {
      const productNameJson = await fetchDataFromApi(
        apiEndpointProduct,
        requestOptionsDelete
      );
      setProductName(productNameJson[0].product_name);
      setMachineName(productNameJson[0].machine_name);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const fetchData = async () => {
    const requestOptions = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        /* Your data goes here */
      }),
      redirect: "follow",
    };

    const apiEndpointSelect = `product/list_process?id=${productId}`;

    try {
      const jsonData = await fetchDataFromApi(
        apiEndpointSelect,
        requestOptions
      );
      if (Array.isArray(jsonData)) {
        setSelectData(jsonData);
      } else {
        console.error("Invalid data format received from API:", jsonData);
      }
    } catch (error) {
      setSelectData([]);
      Alert.alert("Error", "Please Check Network");
      console.error("Error fetching data:", error);
    } finally {
      // Set loading to false once the API request is completed (whether successful or not)
      setIsLoading(false);
    }
  };

  // TODO : ERROR Handle
  const handleDeleteProduct = async () => {
    const requestOptionsDelete = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({}),
      redirect: "follow",
    };

    const apiEndpointDelete = `product/delete?id=${productId}`;

    try {
      const deleteData = await fetchDataFromApi(
        apiEndpointDelete,
        requestOptionsDelete
      );
      if (deleteData.message == "success") {
        ToastAndroid.show("Product Deleted", ToastAndroid.SHORT);
        navigation.navigate("Home");
      } else {
        console.error("Invalid data format received from API:", deleteData);
      }
    } catch (error) {
      ToastAndroid.show(erro, ToastAndroid.SHORT);
      console.error("Error fetching data:", error);
    }
  };

  const handleUpdateProduct = async () => {
    // console.log(productId);
    const convertedData = updatedData.map((item) => [
      item.process_id,
      item.process_name,
      item.time,
      item.cost,
    ]);
    // console.log(convertedData);

    var formdata = new FormData();
    formdata.append("product_id", productId); //Todo : find the Product Name
    formdata.append("product_name", productName);
    formdata.append("machine_name", machineName.toUpperCase());
    formdata.append("process", JSON.stringify(convertedData));

    var requestOptions = {
      method: "POST",
      body: formdata,
      redirect: "follow",
    };

    const apiEndpointUpdate = "product/rename";

    try {
      const jsonUpdateData = await fetchDataFromApi(
        apiEndpointUpdate,
        requestOptions
      );
      // console.log(jsonUpdateData.message);
      if (jsonUpdateData.message == "Success") {
        ToastAndroid.show("Product Updated", ToastAndroid.SHORT);
        navigation.navigate("Home");
      } else {
        console.error("Invalid data format received from API:", jsonUpdateData);
        
      }
    } catch (error) {
      // Handle errors here
      Alert.alert("Error", "Please Check Network");
      ToastAndroid.show(error, ToastAndroid.SHORT);
      console.error("Error fetching data:", error);
    }
  };

  // Generate an array of process input groups based on the processCount state
  const handleProcessChange = (index, value, property) => {
    const updatedSelectData = [...updatedData];
    updatedSelectData[index][property] = value;
    setUpdatedData(updatedSelectData); //error in data not set  this section
  };

  return (
    <SafeAreaView
      style={{
        paddingHorizontal: 20,
        backgroundColor: "#DBE9F6",
        width: "100%",
        height: "100%",
        alignItems: "baseline",
      }}
    >
    <Background/>

      <MyInput
        label="Product Name"
        InputStyle={{ marginVertical: 10 }}
        leading="file"
        leadingIconColor="#7350B8"
        value={productName}
        onTextChange={handleProductNameChange}
      />
      <MyInput
        label="Machine Name"
        InputStyle={{ marginVertical: 10 }}
        leading="file"
        leadingIconColor="#7350B8"
        value={machineName}
        onTextChange={handleMachineNameChange}
      />
      <ScrollView showsVerticalScrollIndicator={false} >
        {isLoading ? (
          <ActivityIndicator size="large" color={primary} />
        ) : selectData.length === 0 ? (
          <Text variant="h6">No Proccess available</Text>
        ) : (
          selectData.map((item, index) => (
            <MyInputGroup
              key={item.process_id}
              style={styles.inputBox}
              label1={`Proccess ${index + 1}`}
              onTextChange={(name) =>
                handleProcessChange(index, name, "process_name")
              }
              onTextChange1={(time) => handleProcessChange(index, time, "time")}
              onTextChange2={(cost) => handleProcessChange(index, cost, "cost")}
              // Set default values from data state
              defaultValue1={item.process_name}
              defaultValue2={item.time}
              defaultValue3={item.cost}
            />
          ))
        )}
      </ScrollView>
      <View style={{marginBottom: 10,flexDirection: "row"}}>

      <MyButton
      buttonStyle={{width: '49%', marginRight: 10}}
        title="Update"
        onPress={handleUpdateProduct}
      />
      <MyButton
        buttonStyle={{width: '49%'}}
        title="Delete"
        icon="trash"
        innerStyle={{ backgroundColor: "#d00000"}}
        onPress={handleDeleteProduct}
      />
      </View>
    </SafeAreaView>
  );
};

export default EditProduct;

const styles = StyleSheet.create({
  inputBox: {
    marginVertical: 5,
  },
});
