import { Alert, SafeAreaView, ScrollView } from "react-native";
import React, { useContext, useEffect, useState } from "react";
import { useTheme } from "../components/Theme";
import { Text } from "@react-native-material/core";
import MyButton from "../components/MyButton";
import CartProduct from "../components/CartProduct";
import { AppContext } from "../components/MyContext";
import { fetchDataFromApi } from "../config/APiConfig";
import { ActivityIndicator } from "@react-native-material/core";
import Background from "../components/Background";


const Cart = ({ navigation }) => {
  const { primary, fontStyles } = useTheme();
  const { cartItems, setCartItems } = useContext(AppContext);
  const { studentName, setStudentName } = useContext(AppContext);
  const [cartProducts, setCartProducts] = useState([]);
  const [isLoading, setIsLoading] = useState(true); // State to track loading status

  useEffect(() => {
    // Fetch data from the API when the component mounts
    getCardDetails();
    // console.log(cartItems);
  }, []); // Add an empty dependency array here to run the effect only once

  const getCardDetails = async () => {
    // Filter out cart items with defined count
    const definedCartItems = cartItems.map((itemGroup) =>
      itemGroup.filter((item) => item.count !== undefined && item.count !== "")
    );
    // console.log(definedCartItems);
    setCartItems(definedCartItems);

    // Retrieve product names and processes for each cart item
    const cartProductsData = [];
    for (const itemGroup of definedCartItems) {
      const cartProductGroup = [];
      for (const process of itemGroup) {
        const productName = await getProductName(process.productId);
        const processData = await getProcess(process.processId);
        cartProductGroup.push({
          productName : productName.product_name,
          processName: processData.process_name,
          machineName: productName.machine_name,
          count: process.count,
          timeTaken: (process.count * processData.time) / 60,
        });
      }
      cartProductsData.push(cartProductGroup);
    }

    //  console.log(cartProductsData);
    setCartProducts(cartProductsData);
    setIsLoading(false);
    // Do whatever you want with cardDetailsObject, for example, setCardDetails(cardDetailsObject);
  };

  const getProductName = async (id) => {
    const requestOptionsProduct = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({}),
      redirect: "follow",
    };

    const apiEndpointProduct = `/details/get_product?id=${id}`;

    try {
      const productNameJson = await fetchDataFromApi(
        apiEndpointProduct,
        requestOptionsProduct
      );
      // console.log(productNameJson);
      return {"product_name" :productNameJson[0].product_name,"machine_name": productNameJson[0].machine_name} ;
    } catch (error) {
      return null;
      console.error("Error fetching data:", error);
    }
  };

  const getProcess = async (id) => {
    const requestOptionsProcess = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({}),
      redirect: "follow",
    };

    const apiEndpointProcess = `/details/get_process?id=${id}`;

    try {
      const processJson = await fetchDataFromApi(
        apiEndpointProcess,
        requestOptionsProcess
      );
      return processJson[0];
    } catch (error) {
      return null;
      console.error("Error fetching data:", error);
    }
  };

  const removeCartItem = (itemIndex, processIndex) => {
    // Create a copy of the cartItems array
    const updatedCartItems = [...cartItems];

    // Remove the specified item and process from the copy
    updatedCartItems[itemIndex].splice(processIndex, 1);

    // Update the state with the updated cartItems array
    setCartItems(updatedCartItems);
  };

  const confirmRegister = async () => {
    if (!studentName.trim()) {
      Alert.alert("Error", "Please Login Again", [{ text: "OK", onPress: () => navigation.navigate('Login') }]);
      return;
    }
    
    // console.log(cartItems);
    
    // Extract the processData array from the cartItems
    const processString = cartItems.flatMap((itemGroup) =>
    itemGroup.map((item) => [item.processId, item.count])
    );
    if (processString.length === 0) {
      Alert.alert("Error", "Your cart is empty.", [{ text: "OK", onPress: () => navigation.navigate('Home') }]);
      return;
    }
    // console.log(processString);

    // Create the FormData object to send the data
    var formdata = new FormData();
    formdata.append("student_name", studentName);
    formdata.append("process", JSON.stringify(processString));

    // Set up the fetch options
    var requestOptions = {
      method: "POST",
      body: formdata,
      redirect: "follow",
    };

    // console.log(formdata);

    const apiEndpoint = "students/add";

    try {
      const jsonData = await fetchDataFromApi(apiEndpoint, requestOptions);
      // console.log(jsonData.Message);
      if (jsonData.Message) {
        setCartItems([]);
        navigation.navigate("Success");
      } else {
        console.error("Invalid data format received from API:", jsonData);
      }
    } catch (error) {
      // Handle errors here
      Alert.alert("Error", "Please Check Network");
      console.error("Error fetching data:", error);
    }
  };

  return (
    <SafeAreaView
      style={{
        paddingHorizontal: 20,
        backgroundColor: "#DBE9F6",
        width: "100%",
        height: "100%",
      }}
    >
    <Background/>

      <Text
        variant="h5"
        style={{
          color: primary,
          ...fontStyles.bold,
          fontSize: 20,
          marginVertical: 15,
        }}
      >
        What you have done
      </Text>
      <ScrollView showsVerticalScrollIndicator={false}>
        {isLoading ? (
          <ActivityIndicator size="large" color={primary} />
        ) : cartProducts.length === 0 ? (
          <Text variant="h6" style={{textAlign: 'center'}}>Cart is Empty</Text>
        ) : (
          cartProducts.map((itemGroup, groupIndex) => (
            <React.Fragment key={groupIndex}>
              {itemGroup.map((process, index) => (
                <CartProduct
                  key={`${groupIndex}_${index}_${process.processId}`}
                  productName={process.productName}
                  processName={process.processName}
                  machineName={process.machineName}
                  count={(process.count)}
                  timeTaken={process.timeTaken.toFixed(2)}
                  style={{ marginVertical: 5 }}
                  onDelete={() => removeCartItem(groupIndex, index)}
                />
              ))}
            </React.Fragment>
          ))
        )}
      </ScrollView>

      <MyButton
        buttonStyle={{ marginBottom: 10 }}
        title="Confirm Register"
        icon="paper-plane"
        onPress={confirmRegister}
      />
    </SafeAreaView>
  );
};

export default Cart;
