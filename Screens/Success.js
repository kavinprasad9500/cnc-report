import { Dimensions, SafeAreaView, StatusBar, StyleSheet } from "react-native";
import React from "react";
import { useTheme } from "../components/Theme";
import { Text } from "@react-native-material/core";
import MyButton from "../components/MyButton";
import LottieView from "lottie-react-native";
import Background from "../components/Background";

const screenWidth = Dimensions.get("window").width;

const Success = ({ navigation }) => {
  const { primary, fontStyles, textBold, textMedium } =
    useTheme();
  // const route = useRoute();
  // const key = route.params?.key;

  const styles = StyleSheet.create({
    headerText: {
      color: textBold,
      ...fontStyles.bold,
    },
    headerTextColored: {
      color: primary,
      ...fontStyles.bold,
    },
    subHeaderText: {
      color: textMedium,
      ...fontStyles.bold,
    },
    lightText: {
      color: "#008000",
      ...fontStyles.bold,
    },
  });
  return (
    <SafeAreaView
      style={{
        justifyContent: "center",
        paddingHorizontal: 20,
        backgroundColor: "#DBE9F6",
        width: "100%",
        alignItems: "center",
        height: "100%",
      }}
    >
    <Background/>

      <LottieView
        source={require("../assets/success.json")}
        style={{
          width: screenWidth - 100,
          height: screenWidth - 100,
        }}
        autoPlay
        loop
      />

      <Text variant="h3" style={styles.headerText}>
        Registeration
      </Text>
      <Text variant="h3" style={styles.headerTextColored}>
        Successfull
      </Text>
      <Text style={styles.subHeaderText}>
        Your Work will check by Admin{"\n\n"}
      </Text>
      <Text variant="h5" style={styles.lightText}>
        Happy to work with you
      </Text>
      <StatusBar style="light" />
      <MyButton
        buttonStyle={{ marginTop: 30 }}
        title="Back to Another Response"
        onPress={() => navigation.navigate("Home")}
      />
    </SafeAreaView>
  );
};
export default Success;
