import React, { useEffect, useState } from "react";
import { View, Text } from "react-native";
import NetInfo from "@react-native-community/netinfo";

const CheckNetwork = () => {
  const [isConnected, setIsConnected] = useState(true); // Default value can be true as an assumption

  useEffect(() => {
    const unsubscribe = NetInfo.addEventListener((state) => {
      setIsConnected(state.isConnected);
    });

    // Cleanup the listener when the component is unmounted
    return () => {
      unsubscribe();
    };
  }, []);

  return !isConnected ? (
    <View style={{ backgroundColor: "red", width: "100%", height: 20 }}>
      <Text style={{ textAlign: "center", color: "#fff" }}>
        Please Connect to Network
      </Text>
    </View>
  ) : null;
};

export default CheckNetwork;
