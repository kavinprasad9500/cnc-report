import React, { useContext } from "react";
import { useNavigation } from "@react-navigation/native";
import { TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import { AppContext } from "./MyContext";

const MyCartButton = () => {
    const navigation = useNavigation();
    const {accessAdmin, setAdmin} = useContext(AppContext);

  return !accessAdmin ? (
    
    <TouchableOpacity onPress={()=>navigation.navigate("Cart")} style={{ marginRight: 25 }}>
      <Icon name="shopping-cart" size={25} color="#fff" />
    </TouchableOpacity>
       
  ) :null;
};
export default MyCartButton;
