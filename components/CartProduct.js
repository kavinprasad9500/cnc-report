// CartProduct.js
import {
  Dimensions,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import React, { useContext, useState } from "react";
import { Divider } from "react-native-flex-layout";
import { useTheme } from "./Theme";
import { Chip } from "@react-native-material/core";
import Icon from "react-native-vector-icons/FontAwesome";
import { AppContext } from "./MyContext";

const CartProduct = (props) => {
  const { fontStyles, background, textLight } = useTheme();
  const { accessAdmin, setAdmin } = useContext(AppContext);

  const [isVisible, setIsVisible] = useState(true); // State to control component visibility

  const handleDeletePress = () => {
    setIsVisible(false);
    props.onDelete();
  };

  const styles = StyleSheet.create({
    box: {
      width: "100%",
      height: 138,
      borderRadius: 10,
      borderWidth: 0.5,
      borderColor: textLight,
      backgroundColor: "#DBE9F6",
      shadowColor: "#000",
      shadowOpacity: 0.5,
      shadowRadius: 4.59,
      elevation: 5,
    },
    innerBox: {
      width: "100%",
      height: 80,
      borderRadius: 10,
      borderWidth: 0.5,
      borderColor: "#B4B4B4",
      backgroundColor: "#DBE9F6",
      flexDirection: "row",
      padding: 10,
    },
    Image: {
      width: 50,
      height: 50,
      margin: 5,
      opacity: 0.2,
    },
    ProductText: {
      fontWeight: "bold", // Apply bold font directly to the Text component
      color: "#000",
      width: "75%",
      ...fontStyles.bold, // Use the fontStyles.bold from the theme for Nunito_700Bold
    },
    ProcessText: {
      fontSize: 12,
      color: textLight,
      fontWeight: "600",
      marginRight: 10,
    },
    CountText: {
      marginTop: 3,
      fontWeight: "700",
    },
    outerBox: {
      paddingHorizontal: 15,
    },
  });

  if (!isVisible) {
    return null;
  }

  return isVisible ? (
    <View style={[styles.box, props.style]}>
      <View style={styles.innerBox}>
        <Image
          source={require("../assets/product-image.png")}
          style={styles.Image}
        />
        <Text style={styles.ProductText}>
          {props.productName}
          {"\n"}
          <View style={{flexDirection: 'row'}}>
            <Text style={styles.ProcessText}>{props.processName}</Text>
            <Chip
              variant="filled"
              label={props.machineName}
              color="#fff"
              contentContainerStyle={{ height: 18 }}
              labelStyle={{ fontSize: 10 }}
              style={{ height: 20, backgroundColor: "#1c77bc" }}
            />
          </View>
        </Text>
        {!accessAdmin ? (
          <TouchableOpacity onPress={handleDeletePress}>
            <Icon name="trash" size={22} color="#d00000" />
          </TouchableOpacity>
        ) : null}
      </View>
      <View style={styles.outerBox}>
        <Text style={styles.CountText}>
          No of Product Process : {props.count}{" "}
        </Text>
        <Divider style={{ marginVertical: 5, backgroundColor: textLight }} />
        <Text style={{ color: textLight }}>
          Time Taken to Complete: {props.timeTaken} (min){" "}
          {accessAdmin ? <Text>Cost : ₹{props.cost}</Text> : null}
        </Text>
      </View>
    </View>
  ) : null;
};

export default CartProduct;
