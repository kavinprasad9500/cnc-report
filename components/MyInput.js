import { TextInput, IconButton } from "@react-native-material/core";
import Icon from "@expo/vector-icons/MaterialCommunityIcons";
import React, { useState } from "react";
import { useTheme } from "./Theme";

const MyInput = (props) => {
  const [text, setText] = useState(props.defaultValue || "");
  const [isPasswordVisible, setPasswordVisible] = useState(false);
  const { primary } = useTheme();

  const togglePasswordVisibility = () => {
    setPasswordVisible((prevVisible) => !prevVisible);
  };

  const handleTextChange = (newText) => {
    setText(newText);
    if (props.onTextChange) {
      props.onTextChange(newText);
    }
  };

  return (
    <TextInput
    inputContainerStyle={{backgroundColor: '#DBE9F6'}}
      style={[props.InputStyle, { width: "100%" , color: primary}]}
      label={props.label}
      variant="outlined"
      leading={(iconProps) => (
        <IconButton
          icon={() => <Icon name={props.leading} {...iconProps} color={props.leadingIconColor}/>} // Use props.leading for the icon name
          {...iconProps}
        />
      )}
      trailing={
        props.trailing
          ? (iconProps) => (
              <IconButton
                icon={() => (
                  <Icon
                    name={
                      isPasswordVisible && props.trailing ? "eye" : "eye-off"
                    }
                    {...iconProps}
                  />
                )}
                onPress={togglePasswordVisibility}
                {...iconProps}
              />
            )
          : null
      }
      secureTextEntry={props.secureTextEntry ? !isPasswordVisible : false}
      value={props.value ? props.value : text}
      onChangeText={handleTextChange}
      editable={!props.disabled}
      keyboardType={(props.keyboardType) ? "numeric": "name-phone-pad"}
    />
  );
};

export default MyInput;
