import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import React, { useContext } from "react";
import { useTheme } from "./Theme";
import { AppContext } from "./MyContext";

const NavigateBar = (props) => {
  const { primary, fontStyles, textMedium } = useTheme();
  const { accessAdmin, setAdmin } = useContext(AppContext);

  return (
    <View style={{ flexDirection: "row", marginVertical: 10 }}>
      <TouchableOpacity onPress={props.componentsNav}>
        <Text
          variant="h5"
          style={{
            color: props.componentTab ? primary : textMedium, // Change color for active tab
            ...fontStyles.bold,
            fontSize: 18,
          }}
        >
          Components {"\t\t"}
        </Text>
      </TouchableOpacity>

      {accessAdmin ? (
        <TouchableOpacity onPress={props.activityNav}>
          <Text
            variant="h5"
            style={{
              color: props.activityTab ? primary : textMedium, // Change color for active tab
              ...fontStyles.bold,
              fontSize: 18,
            }}
          >
            Activities
          </Text>
        </TouchableOpacity>
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({});
export default NavigateBar;
