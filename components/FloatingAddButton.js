import React from "react";
import { StyleSheet, View, TouchableOpacity } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { useTheme } from "./Theme";

const FloatingAddButton = (props) => {
  const { primary } = useTheme();

  const styles = StyleSheet.create({
    float: {
      position: "absolute",
      bottom: 25,
      right: 25,
    },
    button: {
      backgroundColor: primary,
      width: 60,
      height: 60,
      borderRadius: 30,
      alignItems: "center",
      justifyContent: "center",
    },
  });

  return (
    <View style={styles.float}>
      <TouchableOpacity style={styles.button} onPress={props.onPress}>
        <Ionicons name="add-outline" size={40} color={"#ffff"} />
      </TouchableOpacity>
    </View>
  );
};

export default FloatingAddButton;
