import { StyleSheet, View } from "react-native";
import { Button } from "react-native-elements";
import Icon from "react-native-vector-icons/FontAwesome";
import { useTheme } from "./Theme";

const MyButton = (props) => {
  const { primary } = useTheme();
  const styles = StyleSheet.create({
    Button: {
      height: 50,
      backgroundColor: primary,
      borderRadius: 15,
    },
    buttonContainer: {
      width: "100%",
    },
  });
  return (
    <View style={[styles.buttonContainer, props.buttonStyle]}>
      <Button
        buttonStyle={[styles.Button, props.innerStyle]}
        icon={props.icon ? (
          <Icon
            name={props.icon}
            size={20}
            color="white"
            style={{ marginRight: 10 }}
          />
        ) : null}
        title={props.title}
        loading={props.loading}
        onPress={props.onPress}
        titleStyle={{ fontSize: 20 }}
      />
    </View>
  );
};

export default MyButton;
