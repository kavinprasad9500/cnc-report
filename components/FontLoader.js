import { useFonts, Nunito_700Bold } from '@expo-google-fonts/nunito';

const FontLoader = ({ children }) => {
  const [fontsLoaded] = useFonts({
    Nunito_700Bold,
    // Add more custom fonts here if needed
  });

  // Wait for the fonts to be loaded before rendering the children
  if (!fontsLoaded) {
    return null; // You can render a loading indicator or fallback here
  }

  return children;
};

export default FontLoader;
