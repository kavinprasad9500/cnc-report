// customTheme.js
import React, { createContext, useContext, useState } from "react";
import { AppProvider } from "./MyContext";
import FontLoader from "./FontLoader";

const ThemeContext = createContext({
  primary: "#1c77bc",
  background: "#fff",
  textBold: "#525252",
  textMedium: "#6F6F6F",
  textLight: "#7E7E7E",
});

export const useTheme = () => useContext(ThemeContext);

export const ThemeProvider = ({ children }) => {
  const [primary, setPrimary] = useState("#1c77bc");
  const [background, setBackground] = useState("#fff");
  const [textBold, setTextBold] = useState("#6A6B7D");
  const [textMedium, setTextMedium] = useState("#6F6F6F");
  const [textLight, setTextLight] = useState("#7E7E7E");

  // Custom font styles
  const fontStyles = {
    // Define your font styles here
    bold: {
      fontFamily: "Nunito_700Bold",
    },
    // Add more font styles as needed
  };

  return (
    <ThemeContext.Provider
      value={{
        primary,
        background,
        fontStyles,
        textBold,
        textMedium,
        textLight,
      }}
    >
      <FontLoader>
        <AppProvider>{children}</AppProvider>
      </FontLoader>
    </ThemeContext.Provider>
  );
};
