// AppContext.js

import React, { createContext, useState } from 'react';

const AppContext = createContext();

const AppProvider = ({ children }) => {
  const [accessAdmin, setAdmin] = useState(false);
  const [cartItems, setCartItems] = useState([]);
  const [studentName, setStudentName] = useState('');

  return (
    <AppContext.Provider value={{ accessAdmin, setAdmin ,cartItems, setCartItems, studentName, setStudentName }}>
      {children}
    </AppContext.Provider>
  );
};

export { AppContext, AppProvider };
