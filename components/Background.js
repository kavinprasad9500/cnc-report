import { Dimensions, Image } from "react-native";
import React from "react";

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

export default function Background() {
  return (
    <Image
      style={{
        position: "absolute",
        width: screenWidth,
        height: screenHeight,
        opacity: 0.2,
      }}
      source={require("../assets/Doodle.jpg")}
    />
  );
}
