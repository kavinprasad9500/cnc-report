import {
  Dimensions,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import React from "react";
import { useTheme } from "./Theme";
import { Chip } from "@react-native-material/core";

const MyProduct = (props) => {
  const screenWidth = Dimensions.get("window").width;
  const { fontStyles, background, textBold, textLight } = useTheme();

  const styles = StyleSheet.create({
    container: {
      width: screenWidth / 2 - 40,
      height: screenWidth / 2 - 40,
      margin: 10,
      display: "flex",
      borderColor: textLight,
      borderWidth: 0.5,
      borderRadius: 20,
      // backgroundColor: 'transparent',
      backgroundColor: "rgb(219, 233, 246)",
      shadowColor: "#000",
      shadowOffset: {
        width: 1,
        height: 3,
      },
      shadowOpacity: 0.18,
      shadowRadius: 4.59,
      elevation: 5,
    },
    Image: {
      width: screenWidth / 2 - 80,
      height: screenWidth / 2 - 80,
      opacity: 0.3,
    },
    Txt: {
      color: "#000",
      padding: 5,
      fontSize: 13,
      justifyContent: "center",
      ...fontStyles.bold,
      textAlign: "center",
    },
  });

  return (
    <TouchableOpacity style={styles.container} onPress={props.onPress}>
      <Chip
        variant="filled"
        label={props.machine}
        color="#fff"
        contentContainerStyle={{ height: 18 }}
        labelStyle={{ fontSize: 8 }}
        style={{position: 'absolute', height: 18, backgroundColor: "#1c77bc", marginLeft: 10, marginTop: 10 }}
      />
    <View style={{alignItems: 'center', justifyContent: 'center'}}>


      <Image
        style={styles.Image}
        width={screenWidth / 2 - 40}
        source={require("../assets/product-image.png")}
      />
      <Text style={styles.Txt}>{props.productName}</Text>
    </View>
    </TouchableOpacity>
  );
};

export default MyProduct;
