import { StyleSheet, Text } from "react-native";
import React, { useState } from "react";
import { ListItem, Icon, Button } from "react-native-elements";
import { SafeAreaView } from "react-native";
import { useTheme } from "./Theme";

const MyStudentList = (props) => {
  const { primary, fontStyles, textBold, textMedium } = useTheme();
  // const [isVisible, setIsVisible] = useState(true); // State to control component visibility

  const handleDeletePress = () => {
    // setIsVisible(false); // Hide the component when "Delete" is pressed
    if(props.onDelete())
      props.onDelete();
  };
  const styles = StyleSheet.create({
    listBox: {
      backgroundColor: "#DBE9F6",
      width: "100%",
      borderColor: textMedium,
      borderWidth: 0.5,
      borderRadius: 10,
      marginVertical: 5,
    },
  });
  // if (!isVisible) {
  //   return null;
  // }

  return (
    <SafeAreaView style={styles.listBox}>
      <ListItem.Swipeable
        containerStyle={{backgroundColor: '#DBE9F6', borderRadius: 10}}
        leftContent={
          <Button
            title="Info"
            icon={{ name: "info", color: "white" }}
            buttonStyle={{ minHeight: "100%", borderRadius: 10, backgroundColor: '#1c77bc' }}
            onPress={props.onView}

          />
        }
        rightContent={
          <Button
            title="Delete"
            icon={{ name: "delete", color: "white" }}
            buttonStyle={{ minHeight: "100%", backgroundColor: "#d00000", borderRadius: 10 }}
            onPress={handleDeletePress}
          />
        }
      >
        <Text style={{ color: textBold, ...fontStyles }}>{props.date}</Text>
        <ListItem.Content style={{ width: "100%" }}>
          <Text
            style={{
              color: primary,
              ...fontStyles,
              fontWeight: 500,
              fontSize: 16,
            }}
          >
            {props.studentName}
          </Text>
        </ListItem.Content>
        {props.email == 1 ? ( 
          <Icon name="check" type="font-awesome" color="green" size={20} />
        ) : null}
        <ListItem.Chevron />
      </ListItem.Swipeable>
    </SafeAreaView>
  );
};

export default MyStudentList;
