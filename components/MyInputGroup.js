import { TextInput } from "@react-native-material/core";
import React, { useState } from "react";
import { StyleSheet, View } from "react-native";

const MyInputGroup = (props) => {
  const [processName, setProcessName] = useState(props.defaultValue1 || "");
  const [time, setTime] = useState(props.defaultValue2 || "");
  const [cost, setCost] = useState(props.defaultValue3 || "");

  const handleTextChange1 = (processName) => {
    setProcessName(processName);
    if (props.onTextChange) {
      props.onTextChange(processName);
    }
  };

  const handleTextChange2 = (time) => {
    setTime(time);
    if (props.onTextChange1) {
      props.onTextChange1(time);
    }
  };

  const handleTextChange3 = (cost) => {
    setCost(cost);
    if (props.onTextChange2) {
      props.onTextChange2(cost);
    }
  };

  return (
    <View style={[props.style, styles.inputGroup]}>
      <TextInput
      inputContainerStyle={{backgroundColor: '#DBE9F6'}}
        style={{ width: "60%" }}
        label={props.label1}
        variant="outlined"
        value={processName}
        onChangeText={handleTextChange1}
      />
      <TextInput
      inputContainerStyle={{backgroundColor: '#DBE9F6'}}
        style={{ width: "20%", borderRadius: 0 }}
        label="Time(Sec)"
        variant="outlined"
        value={time}
        onChangeText={handleTextChange2}
        keyboardType="numeric"
      />
      <TextInput
      inputContainerStyle={{backgroundColor: '#DBE9F6'}}
        style={{ width: "20%" }}
        label="Cost"
        variant="outlined"
        value={cost}
        onChangeText={handleTextChange3}
        keyboardType="numeric"
      />
      {/* {props.onDelete ? (
        <TouchableOpacity style={styles.deleteButton} onPress={props.onDelete}>
          <Icon name="trash" size={20} color="red" />
        </TouchableOpacity>
      ) : null} */}
    </View>
  );
};

export default MyInputGroup;

const styles = StyleSheet.create({
  inputGroup: {
    flexDirection: "row",
  },
  deleteButton: {
    paddingHorizontal: 5,
    paddingVertical: 12,
  },
});
