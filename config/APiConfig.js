import { Alert, ToastAndroid } from "react-native";

const BASE_URL = "https://cnc.kavinprasad.me/api";

export const fetchDataFromApi = async (endpoint, requestOptions = {}) => {
  try {
    const response = await fetch(`${BASE_URL}/${endpoint}`, requestOptions);
    if (!response.ok) {
      throw new Error(`HTTP error! Status: ${response.status}`);
    }
    const data = await response.json();
    return data;
  } catch (error) {
    ToastAndroid.show(error, ToastAndroid.SHORT);
    Alert.alert("Error", "Please Check Network");
    console.error("Error fetching data from API:", error);
    return null;
  }
};
