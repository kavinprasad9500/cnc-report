// Import the required libraries
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import GetStart from "./Screens/GetStart";
import Login from "./Screens/Login";
import Home from "./Screens/Home";
import { ThemeProvider, useTheme } from "./components/Theme";
import Process from "./Screens/Process";
import Cart from "./Screens/Cart";
import Success from "./Screens/Success";
import Activities from "./Screens/Activities";
import MyCartButton from "./components/MyCartButton";
import AddProduct from "./Screens/AddProduct";
import EditProduct from "./Screens/EditProduct";
import StudentActivities from "./Screens/StudentActivities";
import CheckNetwork from "./components/CheckNetwok";
import { StatusBar } from "react-native";
import Background from './components/Background';
<Background/>
// Create a Stack Navigator
const Stack = createStackNavigator();

// Wrap your App component with the NavigationContainer and the Stack Navigator
export default function App() {
  const { primary, fontStyles, background, textBold, textMedium } = useTheme();

  return (
    <ThemeProvider>
      <StatusBar backgroundColor="#1c77bc" barStyle="light-content" />
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName="GetStart"
          screenOptions={{
            headerStyle: {
              backgroundColor: '#1c77bc', // Set the background color for the app bar
            },
            headerTintColor: "#fff", // Set the text color for the app bar
            headerTitleStyle: {
              fontWeight: 600,
              ...fontStyles,
              color: '#fff'
            },
          }}
        >
          <Stack.Screen
            name="GetStart"
            component={GetStart}
            options={{ headerShown: false, headerStatusBarHeight: 0 }}
          />
          <Stack.Screen
            name="Login"
            component={Login}
            options={{ headerShown: false, headerStatusBarHeight: 0 }}
          />
          <Stack.Screen
            name="Home"
            component={Home}
            options={{
              headerLeft: () => null,
              headerRight: () => <MyCartButton />,
            }}
          />
          <Stack.Screen
            name="Process"
            component={Process}
            options={{ headerRight: () => <MyCartButton /> }}
          />
          <Stack.Screen
            name="Cart"
            component={Cart}
            options={{ headerShown: true, headerStatusBarHeight: 0 }}
          />
          <Stack.Screen
            name="Success"
            component={Success}
            options={{ headerShown: false, headerStatusBarHeight: 0 }}
          />
          <Stack.Screen
            name="Activities"
            component={Activities}
            options={{ headerShown: true, headerStatusBarHeight: 0 }}
          />
          <Stack.Screen
            name="AddProduct"
            component={AddProduct}
            options={{ headerShown: true, headerStatusBarHeight: 0 }}
          />
          <Stack.Screen
            name="EditProduct"
            component={EditProduct}
            options={{ headerShown: true, headerStatusBarHeight: 0 }}
          />
          <Stack.Screen
            name="StudentActivities"
            component={StudentActivities}
            options={{ headerShown: true, headerStatusBarHeight: 0 }}
          />
        </Stack.Navigator>
        <CheckNetwork />
      </NavigationContainer>
    </ThemeProvider>
  );
}
